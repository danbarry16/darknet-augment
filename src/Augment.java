package b.da;

import java.awt.image.BufferedImage;
import java.util.ArrayList;

/**
 * Augment.java
 *
 * An interface for performing data augmentation on a given dataset.
 **/
public abstract class Augment{
  /**
   * createAugment()
   *
   * Attempt to create a augment manipulation for a given name.
   *
   * @param name The name of the augment method.
   * @param rep The replacement method.
   * @return The augment object, otherwise NULL.
   **/
  public static Augment createAugment(String name, Replace rep){
    switch(name){
      case "none" :
        return new AugmentNone(rep);
      case "whole" :
        return new AugmentWhole(rep);
      case "center" :
        return new AugmentCenter(rep);
      case "north" :
        return new AugmentNorth(rep);
      case "east" :
        return new AugmentEast(rep);
      case "south" :
        return new AugmentSouth(rep);
      case "west" :
        return new AugmentWest(rep);
      default :
        return null;
    }
  }

  /**
   * process()
   *
   * Process the given dataset using the augmentation method defined by this class.
   *
   * @param bi The image to be augmented.
   * @param region The region to perform the augmentation for.
   * @return The adapted region based what is visible after the augmentation process.
   **/
  public Region process(BufferedImage bi, Region region){
    throw new RuntimeException("Implement me");
  }
}
