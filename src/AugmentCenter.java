package b.da;

import java.awt.image.BufferedImage;

/**
 * AugmentCenter.java
 *
 * Augment the center of the object.
 **/
public class AugmentCenter extends Augment{
  private Replace rep;

  /**
   * AugmentCenter()
   *
   * Initialise the augmenter.
   *
   * @param rep The replacement method.
   **/
  public AugmentCenter(Replace rep){
    this.rep = rep;
  }

  @Override
  public Region process(BufferedImage bi, Region region){
    Region aRegion = new Region(
      region.getClassID(),
      region.getX(),
      region.getY(),
      region.getW() / 2,
      region.getH() / 2
    );
    for(int y = aRegion.getY1(bi); y < aRegion.getY2(bi); y++){
      for(int x = aRegion.getX1(bi); x < aRegion.getX2(bi); x++){
        rep.process(bi, x, y, region);
      }
    }
    /* Outside of inner obscuration should still be visible */
    return region;
  }
}
