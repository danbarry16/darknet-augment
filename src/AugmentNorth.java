package b.da;

import java.awt.image.BufferedImage;

/**
 * AugmentNorth.java
 *
 * Augment the north of the object.
 **/
public class AugmentNorth extends Augment{
  private Replace rep;

  /**
   * AugmentNorth()
   *
   * Initialise the augmenter.
   *
   * @param rep The replacement method.
   **/
  public AugmentNorth(Replace rep){
    this.rep = rep;
  }

  @Override
  public Region process(BufferedImage bi, Region region){
    Region aRegion = new Region(
      region.getClassID(),
      region.getX(),
      region.getY() - (region.getH() / 4),
      region.getW(),
      region.getH() / 2
    );
    for(int y = aRegion.getY1(bi); y < aRegion.getY2(bi); y++){
      for(int x = aRegion.getX1(bi); x < aRegion.getX2(bi); x++){
        rep.process(bi, x, y, region);
      }
    }
    /* Opposite side is visible */
    Region bRegion = new Region(
      region.getClassID(),
      region.getX(),
      region.getY() + (region.getH() / 4),
      region.getW(),
      region.getH() / 2
    );
    return bRegion;
  }
}
