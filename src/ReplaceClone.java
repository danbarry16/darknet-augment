package b.da;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Random;

/**
 * ReplaceClone.java
 *
 * Replaces a given location by finding an appropriate region to swap in, that
 * is not currently mapped by any other regions.
 **/
public class ReplaceClone extends Replace{
  private ArrayList<Region> regions;
  private BufferedImage current;
  private Region replace;
  private ReplaceLocal backup;

  /**
   * ReplaceClone()
   *
   * Initialise the replacement method.
   *
   * @param regions The other regions located within the image.
   **/
  public ReplaceClone(ArrayList<Region> regions){
    this.regions = regions;
    current = null;
    replace = null;
    backup = null;
  }

  @Override
  public BufferedImage process(BufferedImage bi, int x, int y, Region region){
    /* Check if we are still working on the expected image */
    if(current != bi){
      Random rand = new Random();
      current = bi;
      replace = null;
      backup = null;
      /* Try a maximum of 100 times */
      for(int r = 0; r < 100 && replace == null; r++){
        /* Generate a random proposal region */
        double xp = (region.getW() / 2) + (rand.nextDouble() * (1 - region.getW()));
        double yp = (region.getH() / 2) + (rand.nextDouble() * (1 - region.getH()));
        Region proposal = new Region(region.getClassID(), xp, yp, region.getW(), region.getH());
        /* Test it does not overlap with existing regions */
        replace = proposal;
        for(Region reg : regions){
          if(reg.overlaps(proposal)){
            replace = null;
            break;
          }
        }
      }
      /* Generate a backup solution if a region was not found */
      if(replace == null){
        backup = new ReplaceLocal();
      }
    }
    /* Test which method we are using */
    if(backup != null){
      /* Run the local replacement process as a better solution could not be found */
      return backup.process(bi, x, y, region);
    }else{
      /* Calculate offsets */
      int xo = replace.getX1(bi) - region.getX1(bi);
      int yo = replace.getY1(bi) - region.getY1(bi);
      /* Pull the clone region */
      bi.setRGB(x, y, bi.getRGB(x + xo, y + yo));
    }
    return bi;
  }
}
