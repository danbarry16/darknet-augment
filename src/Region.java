package b.da;

import java.awt.image.BufferedImage;

/**
 * Region.java
 *
 * A data structure for a region.
 **/
public class Region{
  private int c;
  private double x;
  private double y;
  private double w;
  private double h;

  /**
   * Region()
   *
   * The region of the object classification. Coordinates are relative to the
   * image, where (0,0) if top left, and (1,1) is bottom right.
   *
   * @param c The object classification index, starting from zero.
   * @param x The centre X of the region relative to the image.
   * @param y The centre Y of the region relative to the image.
   * @param w The width of the region relative to the image.
   * @param h The height of the region relative to the image.
   **/
  public Region(int c, double x, double y, double w, double h){
    this.c = c;
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
  }

  /**
   * getClassID()
   *
   * Get the classification for this object.
   *
   * @return The classification for this object region.
   **/
  public int getClassID(){
    return c;
  }

  /**
   * getX()
   *
   * Get the relative centre X position of the object.
   *
   * @return The relative centre X position.
   **/
  public double getX(){
    return x;
  }

  /**
   * getY()
   *
   * Get the relative centre Y position of the object.
   *
   * @return The relative centre Y position.
   **/
  public double getY(){
    return y;
  }

  /**
   * getW()
   *
   * Get the relative width of the object.
   *
   * @return The relative width.
   **/
  public double getW(){
    return w;
  }

  /**
   * getH()
   *
   * Get the relative height of the object.
   *
   * @return The relative height.
   **/
  public double getH(){
    return h;
  }

  /**
   * getX1()
   *
   * Get the upper left X pixel of the bounding box.
   *
   * @param bi The image to calculate relative to.
   * @return The upper left X pixel of the object's bounding box.
   **/
  public int getX1(BufferedImage bi){
    int p = (int)(get_x1() * bi.getWidth());
    if(p < 0) p = 0;
    else if(p > bi.getWidth()) p = bi.getWidth() - 1;
    return p;
  }

  /**
   * getY1()
   *
   * Get the upper left Y pixel of the bounding box.
   *
   * @param bi The image to calculate relative to.
   * @return The upper left Y pixel of the object's bounding box.
   **/
  public int getY1(BufferedImage bi){
    int p = (int)(get_y1() * bi.getHeight());
    if(p < 0) p = 0;
    else if(p > bi.getHeight()) p = bi.getHeight() - 1;
    return p;
  }

  /**
   * getX2()
   *
   * Get the lower right X pixel of the bounding box.
   *
   * @param bi The image to calculate relative to.
   * @return The lower right X pixel of the object's bounding box.
   **/
  public int getX2(BufferedImage bi){
    int p = (int)(get_x2() * bi.getWidth());
    if(p < 0) p = 0;
    else if(p > bi.getWidth()) p = bi.getWidth() - 1;
    return p;
  }

  /**
   * getY2()
   *
   * Get the lower right Y pixel of the bounding box.
   *
   * @param bi The image to calculate relative to.
   * @return The lower right Y pixel of the object's bounding box.
   **/
  public int getY2(BufferedImage bi){
    int p = (int)(get_y2() * bi.getHeight());
    if(p < 0) p = 0;
    else if(p > bi.getHeight()) p = bi.getHeight() - 1;
    return p;
  }

  /**
   * get_x1()
   *
   * Get the X1 upper left location.
   *
   * @return The relative X1 upper left location.
   **/
  public double get_x1(){ return getX() - (getW() / 2); }

  /**
   * get_y1()
   *
   * Get the Y1 upper left location.
   *
   * @return The relative Y1 upper left location.
   **/
  public double get_y1(){ return getY() - (getH() / 2); }

  /**
   * get_x2()
   *
   * Get the X2 lower right location.
   *
   * @return The relative X2 lower right location.
   **/
  public double get_x2(){ return getX() + (getW() / 2); }

  /**
   * get_y2()
   *
   * Get the Y2 lower right location.
   *
   * @return The relative Y2 lower right location.
   **/
  public double get_y2(){ return getY() + (getH() / 2); }

  /**
   * overlaps()
   *
   * Check whether two regions overlap one another.
   *
   * @param that The region to check against.
   * @return True if the regions overlap, otherwise false.
   **/
  public boolean overlaps(Region that){
    if(this.get_y2() < that.get_y1() || this.get_y1() > that.get_y2()) return false;
    if(this.get_x2() < that.get_x1() || this.get_x1() > that.get_x2()) return false;
    return true;
  }
}
