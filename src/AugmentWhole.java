package b.da;

import java.awt.image.BufferedImage;

/**
 * AugmentWhole.java
 *
 * Augment the entire object.
 **/
public class AugmentWhole extends Augment{
  private Replace rep;

  /**
   * AugmentWhole()
   *
   * Initialise the augmenter.
   *
   * @param rep The replacement method.
   **/
  public AugmentWhole(Replace rep){
    this.rep = rep;
  }

  @Override
  public Region process(BufferedImage bi, Region region){
    for(int y = region.getY1(bi); y < region.getY2(bi); y++){
      for(int x = region.getX1(bi); x < region.getX2(bi); x++){
        rep.process(bi, x, y, region);
      }
    }
    /* All of region is obscured */
    return null;
  }
}
