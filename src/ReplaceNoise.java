package b.da;

import java.awt.image.BufferedImage;

/**
 * ReplaceNoise.java
 *
 * Replaces pixels with random noise.
 **/
public class ReplaceNoise extends Replace{
  /**
   * ReplaceNoise()
   *
   * Initialise the replacement method.
   **/
  public ReplaceNoise(){
    /* Do nothing */
  }

  @Override
  public BufferedImage process(BufferedImage bi, int x, int y, Region region){
    int p = (int)(Math.random() * 256) << 16 |
            (int)(Math.random() * 256) <<  8 |
            (int)(Math.random() * 256);
    bi.setRGB(x, y, p);
    return bi;
  }
}
