package b.da;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriter;
import javax.imageio.ImageWriteParam;
import javax.imageio.plugins.jpeg.JPEGImageWriteParam;
import javax.imageio.stream.FileImageOutputStream;

/**
 * DataSet.java
 *
 * Process and check a given Darknet dataset, ensuring it is ready for use.
 * This class then offers methods useful for processing the dataset.
 **/
public class DataSet{
  /**
   * Triplet.DataSet.java
   *
   * Internal data structure for grouping related data.
   **/
  private class Triplet<A, B, C>{
    public A a;
    public B b;
    public C c;

    public Triplet(A a, B b, C c){
      this.a = a;
      this.b = b;
      this.c = c;
    }
  }

  private JSON config;
  private ArrayList<Triplet<String, File, File>> data;
  private int dataIndex;
  private ArrayList<String> outImages;

  /**
   * DataSet()
   *
   * Initialise the dataset and perform basic checks to ensure we will be able
   * to process later.
   *
   * @param config A configuration describing the tests to be run.
   **/
  public DataSet(JSON config){
    /* Setup internal variables */
    this.config = config;
    data = new ArrayList<Triplet<String, File, File>>();
    dataIndex = -1;
    outImages = new ArrayList<String>();
    /* Test important directories in place */
    File inDir = new File(config.get("in-dir").value(""));
    File outDir = new File(config.get("out-dir").value(""));
    /* Check directories exist */
    File dirs[] = new File[]{ inDir, outDir };
    for(File f : dirs){
      if(!f.exists() || !f.isDirectory()){
        Main.err("Could not find directory '" + f.toString() + "'");
      }
    }
    /* Generate a list of files and add them to the list */
    HashMap<String, Triplet> dataLut = new HashMap<String, Triplet>();
    for(File f : inDir.listFiles()){
      String fn = f.toString();
      int i = fn.lastIndexOf(".");
      if(i >= 0 && i < fn.length()){
        String ext = fn.toString().substring(i + 1).toLowerCase();
        int dir = fn.lastIndexOf("/") + 1;
        dir = dir < fn.toString().length() ? dir : 0;
        String name = fn.toString().substring(dir, i);
        switch(ext){
          case "gif" :
          case "jpg" :
          case "jpeg" :
          case "png" :
            if(dataLut.get(name) == null){
              dataLut.put(name, new Triplet(name, null, null));
              data.add(dataLut.get(name));
            }
            dataLut.get(name).b = new File(fn);
            Main.log("Adding image '" + name + "'");
            break;
          case "txt" :
            if(dataLut.get(name) == null){
              dataLut.put(name, new Triplet(name, null, null));
              data.add(dataLut.get(name));
            }
            dataLut.get(name).c = new File(fn);
            Main.log("Adding labels '" + name + "'");
            break;
          default :
            Main.log("Could not recognise '" + fn + "'");
            break;
        }
      }else{
        Main.log("Skipping '" + fn + "' as extension not found");
      }
    }
    /* Remove entries without images */
    int noLabelFileCount = 0;
    for(Triplet t : new ArrayList<>(data)){
      if(t.a == null || t.b == null){
        data.remove(t);
      }else{
        if(t.b == null){
          noLabelFileCount++;
        }
      }
    }
    /* Print final statistics from loading */
    Main.log("Added a total of " + data.size() + " YOLO images");
    Main.log(noLabelFileCount + " were missing label files");
  }

  /**
   * nextImage()
   *
   * Progress to the next image and get an appropriate name.
   *
   * @return The name of the image, otherwise NULL if none available.
   **/
  public String nextImage(){
    ++dataIndex;
    if(dataIndex < data.size()){
      return data.get(dataIndex).a;
    }
    return null;
  }

  /**
   * getImage()
   *
   * Get a clone of the currently focussed image.
   *
   * @return A clone of the currently focussed image.
   **/
  public BufferedImage getImage(){
    if(dataIndex < 0) nextImage();
    if(dataIndex < data.size()){
      try{
        return ImageIO.read(data.get(dataIndex).b);
      }catch(IOException e){
        Main.err("Failed to load image '" + data.get(dataIndex).a + "'");
      }
    }
    return null;
  }

  /**
   * getRegions()
   *
   * Get the object regions within the image.
   *
   * @return A list of object regions within the image.
   **/
  public ArrayList<Region> getRegions(){
    if(dataIndex < 0) nextImage();
    ArrayList<Region> regions = new ArrayList<Region>();
    if(dataIndex < data.size() && data.get(dataIndex).c != null){
      try{
        BufferedReader br = new BufferedReader(new FileReader(data.get(dataIndex).c));
        String line;
        while((line = br.readLine()) != null){
          String[] components = line.split(" ");
          if(components.length != 5){
            Main.err("Bad label line in '" + data.get(dataIndex).a + "' -> " + line);
          }
          try{
            regions.add(
              new Region(
                Integer.parseInt(components[0]),
                Double.parseDouble(components[1]),
                Double.parseDouble(components[2]),
                Double.parseDouble(components[3]),
                Double.parseDouble(components[4])
              )
            );
          }catch(NumberFormatException e){
            Main.err("Bad number in '" + data.get(dataIndex).a + "' -> " + line);
          }
        }
        br.close();
      }catch(IOException e){
        Main.err("Failed to read labels '" + data.get(dataIndex).a + "'");
      }
    }
    return regions;
  }

  /**
   * saveImage()
   *
   * Save an augmented image to the disk, with a given unique name.
   *
   * @param filename The filename to be assigned to the image and label
   * (without the extension).
   * @param bi The image to be saved.
   * @param regions The regions to be saved.
   **/
  public void saveImage(String filename, BufferedImage bi, ArrayList<Region> regions){
    String fn = config.get("out-dir").value("") + "/" + filename;
    /* Check if debug enabled */
    if(config.exists("debug")){
      JSON debug = config.get("debug");
      /* Check if region enabled, draw them as overlay */
      if(debug.exists("draw-region")){
        for(Region reg : regions){
          if(reg == null) continue;
          for(int y = reg.getY1(bi); y < reg.getY2(bi); y++){
            bi.setRGB(reg.getX1(bi), y, 0);
            bi.setRGB(reg.getX2(bi), y, 0);
          }
          for(int x = reg.getX1(bi); x < reg.getX2(bi); x++){
            bi.setRGB(x, reg.getY1(bi), 0);
            bi.setRGB(x, reg.getY2(bi), 0);
          }
        }
      }
    }
    /* Save the image */
    try{
      JPEGImageWriteParam jpegParams = new JPEGImageWriteParam(null);
      jpegParams.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
      jpegParams.setCompressionQuality(1.0f);
      final ImageWriter writer = ImageIO.getImageWritersByFormatName("jpg").next();
      writer.setOutput(new FileImageOutputStream(new File(fn + ".jpg")));
      writer.write(null, new IIOImage(bi, null, null), jpegParams);
      outImages.add(fn + ".jpg");
    }catch(IOException e){
      Main.err("Could not write image '" + fn + "'");
    }
    /* Save label file */
    try{
      BufferedWriter bw = new BufferedWriter(new FileWriter(new File(fn + ".txt")));
      for(int x = 0; x < regions.size(); x++){
        Region reg = regions.get(x);
        /* Ensure the region wasn't remove during augmentation */
        if(reg != null){
          bw.write(
            reg.getClassID() + " " +
            reg.getX() + " " +
            reg.getY() + " " +
            reg.getW() + " " +
            reg.getH() + "\n"
          );
        }
      }
      bw.close();
    }catch(IOException e){
      Main.err("Could not write labels '" + fn + "'");
    }
    Main.log("Wrote YOLO data for '" + fn + "'");
  }

  /**
   * finalise()
   *
   * Finalise the newly generated dataset.
   **/
  public void finalise(){
    String fn = config.get("out-dir").value("") + "/data.txt";
    /* Save list of images for use in train files */
    try{
      BufferedWriter bw = new BufferedWriter(new FileWriter(new File(fn)));
      for(String i : outImages) bw.write(i + "\n");
      bw.close();
    }catch(IOException e){
      Main.err("Could not write data file");
    }
    Main.log("Wrote data file '" + fn + "'");
  }
}
