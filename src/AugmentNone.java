package b.da;

import java.awt.image.BufferedImage;

/**
 * AugmentNone.java
 *
 * Base case for data augmentation, none applied.
 **/
public class AugmentNone extends Augment{
  private Replace rep;

  /**
   * AugmentNone()
   *
   * Initialise the augmenter.
   *
   * @param rep The replacement method.
   **/
  public AugmentNone(Replace rep){
    this.rep = rep;
  }

  @Override
  public Region process(BufferedImage bi, Region region){
    /* All of region should be visible */
    return region;
  }
}
