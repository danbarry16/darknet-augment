package b.da;

import java.awt.image.BufferedImage;

/**
 * ReplaceGlobal.java
 *
 * Replaces pixels with a random pixel in the image.
 **/
public class ReplaceGlobal extends Replace{
  /**
   * ReplaceGlobal()
   *
   * Initialise the replacement method.
   **/
  public ReplaceGlobal(){
    /* Do nothing */
  }

  @Override
  public BufferedImage process(BufferedImage bi, int x, int y, Region region){
    bi.setRGB(x, y, bi.getRGB((int)(Math.random() * bi.getWidth()),
                              (int)(Math.random() * bi.getHeight())));
    return bi;
  }
}
