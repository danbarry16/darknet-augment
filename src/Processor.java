package b.da;

import java.awt.image.BufferedImage;
import java.util.ArrayList;

/**
 * Processor.java
 *
 * Take a given dataset and run the appropriate augmentation techniques over
 * each object.
 **/
public class Processor{
  private JSON config;
  private DataSet ds;

  /**
   * Processor()
   *
   * Initialise the processor with the configuration.
   **/
  public Processor(JSON config){
    this.config = config;
    ds = new DataSet(config);
  }

  /**
   * run()
   *
   * Run the processor.
   **/
  public void run(){
    /* Loop image variations */
    String filename = null;
    while((filename = ds.nextImage()) != null){
      ArrayList<Region> regions = ds.getRegions();
      /* If regions given, just save the original data and move on */
      if(regions.size() <= 0){
        BufferedImage bi = ds.getImage();
        ds.saveImage(filename, bi, regions);
        continue;
      }
      /* Otherwise let's generate some variations */
      ArrayList<ArrayList<Augment>> augs = generateAugmentationVariations(regions);
      /* Sanity check that the generated augmentations still have the same depth as regions */
      if(augs.size() != regions.size()){
        Main.err("Augmentation (size: " + augs.size() + ") != regions (size: " + regions.size() + ")");
      }
      int augWidth = augs.get(0).size();
      /* Apply an augmentation per region */
      for(int x = 0; x < augWidth; x++){
        ArrayList<Region> oRegions = new ArrayList<Region>();
        BufferedImage bi = ds.getImage();
        /* Perform each of the augmentations */
        for(int y = 0; y < augs.size(); y++){
          oRegions.add(augs.get(y).get(x).process(bi, regions.get(y)));
        }
        /* Save the new variation to disk */
        ds.saveImage(filename + "-" + x, bi, oRegions);
      }
    }
    /* Run finalisation steps */
    ds.finalise();
  }

  /**
   * generateAugmentationVariations()
   *
   * It's possible that we use mixed augmentation methods in the same image
   * based on the region object classes we wish to vary. Each type of
   * augmentation may produce several different results, so we need some form
   * of process to ensure that for each variation, some type of the
   * augmentation is represented as fairly as possible.
   *
   * Ideally we don't want to create lots of augmented images with only one
   * variation between them as it's a waste of disk space and a waste of
   * training time. Each image should be as different as the variation testing
   * will allow.
   **/
  private ArrayList<ArrayList<Augment>> generateAugmentationVariations(
    ArrayList<Region> regions
  ){
    ArrayList<ArrayList<Augment>> variations = new ArrayList<ArrayList<Augment>>();
    int maxAugsWidth = 0;
    /* For each region, find the required variations */
    for(Region reg : regions){
      /* Get the class name and check it is valid */
      int c = reg.getClassID();
      String className = getClassName(config, c);
      if(className == null){
        Main.err("Unknown class name for object ID '" + c + "'");
      }
      JSON augs = config.get("augmentation");
      /* Get the class augmentation method defaults */
      JSON augsDef = augs.get("default");
      String[] augmentations = new String[augsDef.get("occlusions").length()];
      for(int x = 0; x < augmentations.length; x++)
        augmentations[x] = augsDef.get("occlusions").get(x).value(null);
      String replacement = augsDef.get("replacement").value("none");
      /* Check for object specific class augmentation to replace default */
      if(augs.exists(className)){
        JSON augsObj = augs.get(className);
        if(augsObj.exists("occlusions")){
          augmentations = new String[augsObj.get("occlusions").length()];
          for(int x = 0; x < augmentations.length; x++)
            augmentations[x] = augsObj.get("occlusions").get(x).value(null);
        }
        if(augsObj.exists("replacement")){
          replacement = augsObj.get("replacement").value("none");
        }
      }
      /* Get the replacement method */
      Replace r = Replace.createReplace(replacement, regions);
      if(r == null){
        Main.err("Unknown replacement method '" + replacement + "'");
      }
      /* Initialise the augmentations with their replacement methods */
      ArrayList<Augment> augmentors = new ArrayList<Augment>();
      for(int x = 0; x < augmentations.length; x++){
        Augment a = Augment.createAugment(augmentations[x], r);
        if(a != null){
          augmentors.add(a);
        }else{
          Main.err("Unknown augmentation method '" + augmentations[x] + "'");
        }
      }
      variations.add(augmentors);
      /* We want to normalise by the widest augmentation set */
      if(augmentations.length > maxAugsWidth){
        maxAugsWidth = augmentations.length;
      }
    }
    /* Normalise the augmentation methods */
    for(int x = 0; x < variations.size(); x++){
      ArrayList<Augment> var = variations.get(x);
      /* Check if widening required */
      if(var.size() < maxAugsWidth){
        /* Fill up using the previous variations */
        int i = 0;
        int l = var.size();
        while(var.size() < maxAugsWidth){
          var.add(var.get(i++ % l));
        }
      }
      /* Sanity check that we did correctly normalise to the width */
      if(var.size() != maxAugsWidth){
        Main.err("Normalisation failed");
      }
    }
    return variations;
  }

  /**
   * getClassName()
   *
   * Get the class name for a given class ID, otherwise return NULL.
   *
   * @param config The shared configuration.
   * @param id The class ID to be searched for.
   * @return The name associated with the class, otherwise NULL.
   **/
  private String getClassName(JSON config, int id){
    JSON classes = config.get("classes");
    for(int x = 0; x < classes.length(); x++){
      JSON classDesc = classes.get(x);
      int classID = Integer.parseInt(classDesc.get("id").value("-1"));
      if(classID == id){
        return classDesc.get("object").value(null);
      }
    }
    return null;
  }
}
