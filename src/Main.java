package b.da;

/**
 * Main.java
 *
 * Process command line arguments and decide the further execution of the
 * program.
 **/
public class Main{
  private JSON config;

  /**
   * Main()
   *
   * Process the command line arguments and start the program as required.
   *
   * @param args The command line arguments.
   **/
  public Main(String[] args){
    /* Setup the fields */
    config = null;
    /* Iterate over the command line arguments */
    for(int x = 0; x < args.length; x++){
      switch(args[x]){
        case "-c" :
        case "--config" :
          x = config(args, x);
          break;
        case "-h" :
        case "--help" :
          x = help(args, x);
          break;
        default :
          Main.err("Unknown option '" + args[x] + "'");
          break;
      }
    }
    /* Check if we should be processing */
    if(config != null){
      Processor proc = new Processor(config);
      proc.run();
    }
  }

  /**
   * config()
   *
   * Set the configuration.
   *
   * @param args The command line arguments.
   * @param x The current offset into the command line arguments.
   * @return The command line argument offset.
   **/
  private int config(String[] args, int x){
    if(++x >= args.length){
      Main.err("Missing argument to configuration");
    }
    try{
      this.config = JSON.build(args[x]);
    }catch(Exception e){
      Main.err("Unable to read JSON configuration from '" + args[x] + "'");
    }
    return x;
  }

  /**
   * help()
   *
   * Display the help.
   *
   * @param args The command line arguments.
   * @param x The current offset into the command line arguments.
   * @return The command line argument offset.
   **/
  private int help(String[] args, int x){
    System.out.println("java -jar dist/darknet-augment.jar [OPT]");
    System.out.println("");
    System.out.println("  OPTions");
    System.out.println("");
    System.out.println("    -c  --config      Set the configuration file to be used");
    System.out.println("                        <STR> Location of the JSON configuration file");
    System.out.println("    -h  --help        Display this help");
    System.exit(0);
    return x;
  }

  /**
   * formattedOutput()
   **/
  private static void formattedOutput(char pre, String msg){
    System.err.println("[" + System.currentTimeMillis() + "] <" + pre + pre + "> " + msg);
  }

  /**
   * log()
   *
   * Log a formatted message.
   *
   * @param msg The message to be logged.
   **/
  public static void log(String msg){
    formattedOutput('+', msg);
  }

  /**
   * err()
   *
   * Log a formatted error message and halt execution by returning an error
   * code.
   *
   * @param msg The message to be logged.
   **/
  public static void err(String msg){
    formattedOutput('!', msg);
    System.exit(1);
  }

  /**
   * main()
   *
   * Create an object instance context and start the program.
   *
   * @param args The command line arguments.
   **/
  public static void main(String[] args){
    new Main(args);
  }
}
