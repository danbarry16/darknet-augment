package b.da;

import java.awt.image.BufferedImage;

/**
 * AugmentEast.java
 *
 * Augment the east of the object.
 **/
public class AugmentEast extends Augment{
  private Replace rep;

  /**
   * AugmentEast()
   *
   * Initialise the augmenter.
   *
   * @param rep The replacement method.
   **/
  public AugmentEast(Replace rep){
    this.rep = rep;
  }

  @Override
  public Region process(BufferedImage bi, Region region){
    Region aRegion = new Region(
      region.getClassID(),
      region.getX() + (region.getW() / 4),
      region.getY(),
      region.getW() / 2,
      region.getH()
    );
    for(int y = aRegion.getY1(bi); y < aRegion.getY2(bi); y++){
      for(int x = aRegion.getX1(bi); x < aRegion.getX2(bi); x++){
        rep.process(bi, x, y, region);
      }
    }
    /* Opposite side is visible */
    Region bRegion = new Region(
      region.getClassID(),
      region.getX() - (region.getW() / 4),
      region.getY(),
      region.getW() / 2,
      region.getH()
    );
    return bRegion;
  }
}
