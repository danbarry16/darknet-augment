package b.da;

import java.awt.image.BufferedImage;

/**
 * ReplaceLocal.java
 *
 * Replaces pixels with a random local pixel in the image.
 **/
public class ReplaceLocal extends Replace{
  /**
   * ReplaceLocal()
   *
   * Initialise the replacement method.
   **/
  public ReplaceLocal(){
    /* Do nothing */
  }

  @Override
  public BufferedImage process(BufferedImage bi, int x, int y, Region region){
    /* Get inner region */
    int x1 = region.getX1(bi);
    int y1 = region.getY1(bi);
    int x2 = region.getX2(bi);
    int y2 = region.getY2(bi);
    /* Generate outer region */
    Region oRegion = new Region(
      region.getClassID(),
      region.getX(),
      region.getY(),
      region.getW() * 2,
      region.getH() * 2
    );
    int ox1 = oRegion.getX1(bi);
    int oy1 = oRegion.getY1(bi);
    int ox2 = oRegion.getX2(bi);
    int oy2 = oRegion.getY2(bi);
    /* Set default value as random pixel in image as the next best default */
    int _x = (int)(Math.random() * bi.getWidth());
    int _y = (int)(Math.random() * bi.getHeight());
    /* Try a maximum of 100 times */
    for(int r = 0; r < 100; r++){
      /* Generate pixel in outer region */
      int i = (int)(Math.random() * (ox2 - ox1)) + ox1;
      int j = (int)(Math.random() * (oy2 - oy1)) + oy1;
      /* Make sure not in inner region */
      if(i <= x1 || i >= x2 || j <= y1 && j >= y2){
        _x = i;
        _y = j;
        break;
      }
    }
    bi.setRGB(x, y, bi.getRGB(_x, _y));
    return bi;
  }
}
