package b.da;

import java.awt.image.BufferedImage;

/**
 * ReplaceNone.java
 *
 * Replaces none of the pixels.
 **/
public class ReplaceNone extends Replace{
  /**
   * ReplaceNone()
   *
   * Initialise the replacement method.
   **/
  public ReplaceNone(){
    /* Do nothing */
  }

  @Override
  public BufferedImage process(BufferedImage bi, int x, int y, Region region){
    /* Do nothing */
    return bi;
  }
}
