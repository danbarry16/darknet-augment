package b.da;

import java.awt.image.BufferedImage;
import java.util.ArrayList;

/**
 * Replace.java
 *
 * An interface for performing pixel manipulation for a augmentation region.
 **/
public abstract class Replace{
  /**
   * createReplace()
   *
   * Attempt to create a replace manipulation for a given name.
   *
   * @param name The name of the replacement method.
   * @param regions Some replace methods need to know where the other regions
   * are as not to use them in their replacement approach.
   * @return The replacement object, otherwise NULL.
   **/
  public static Replace createReplace(String name, ArrayList<Region> regions){
    switch(name){
      case "none" :
        return new ReplaceNone();
      case "black" :
        return new ReplaceBlack();
      case "noise" :
        return new ReplaceNoise();
      case "global" :
        return new ReplaceGlobal();
      case "local" :
        return new ReplaceLocal();
      case "clone" :
        return new ReplaceClone(regions);
      default :
        return null;
    }
  }

  /**
   * process()
   *
   * Process the given pixel using the replacement method defined by this class.
   *
   * @param bi The image to be augmented.
   * @param x The X location to perform the augmentation.
   * @param y The Y location to perform the augmentation.
   * @param region The region to perform the augmentation for.
   * @return The modified image (the same as bi).
   **/
  public BufferedImage process(BufferedImage bi, int x, int y, Region region){
    throw new RuntimeException("Implement me");
  }
}
