package b.da;

import java.awt.image.BufferedImage;

/**
 * ReplaceBlack.java
 *
 * Replaces pixels with black.
 **/
public class ReplaceBlack extends Replace{
  /**
   * ReplaceBlack()
   *
   * Initialise the replacement method.
   **/
  public ReplaceBlack(){
    /* Do nothing */
  }

  @Override
  public BufferedImage process(BufferedImage bi, int x, int y, Region region){
    bi.setRGB(x, y, 0);
    return bi;
  }
}
